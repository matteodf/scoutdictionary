package utility.com.scoutdictionary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String languagefrom;
    private String languageto;
    private String word;
    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinnerWord;
    private Button btntranslate;
    private EditText edtresult;

    public void setWord(String language) {
        spinnerWord = (Spinner) findViewById(R.id.spinnerInput);
        ArrayAdapter<String> adapter = null;
        switch(language) {
            case "German": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arraytedesco);break;
            case "English": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayinglese);break;
            case "French": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayfrancese);break;
            case "Italian": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayitaliano);break;
            case "Esperanto": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayesperanto);break;
            case "Portuguese": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayportoghese);break;
            case "Spanish": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayspagnolo);break;
            case "Dutch": adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayolandese);break;
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWord.setAdapter(adapter);
        spinnerWord.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                word = (String) spinnerWord.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Toast.makeText(MainActivity.this, "Select one item!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setResult(String[] arrayfrom, String[] arrayto) {

        Log.d("tedesco", String.valueOf(arraytedesco.length));
        Log.d("inglese", String.valueOf(arrayinglese.length));
        Log.d("francese", String.valueOf(arrayfrancese.length));
        Log.d("italiano", String.valueOf(arrayitaliano.length));
        Log.d("esperanto", String.valueOf(arrayesperanto.length));
        Log.d("portoghese", String.valueOf(arrayportoghese.length));
        Log.d("spagnolo", String.valueOf(arrayspagnolo.length));
        Log.d("olandese", String.valueOf(arrayolandese.length));

        for (int i = 0; i < arrayto.length; i++) {
            if (arrayfrom[i].trim().equals(word)) {
                if(i >= 0) {
                    edtresult.setText("");
                    edtresult.setText(arrayto[i]);
                    break;
                }
                else Toast.makeText(MainActivity.this, "Word not found!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String[] arraytedesco = new String[]{
            "Abfallgrube", "Abzeichen", "zweiter Grad-Abzeichen", "erster Grad-Abzeichen", "Akela (m)",
            "Akela (f)", "Armzeichen", "Banner", "Barret",
            "Beil", "Bund; Verband", "Bund (Seil-)", "Bundesamt",
            "Bundesvorsitzender", "Bundesvorsitzende", "Dolmetscher", "Donnerbalken; Latrine",
            "Erprobungen", "eine Probe ablegen", "Erste Hilfe", "Flagge", "Frühstück",
            "Führer", "Gilde; Trupp", "Gildenheim; Truppheim", "Gildenrat; Trupprat",
            "Gildenführer; Truppführer", "Gildenassistent; Truppass.", "Grosses Geheul", "Gürtel",
            "Haik", "Halstuch", "Halstuchknoten", "Heimabend",
            "Hemd", "Hilfssippenführer", "Hut", "Internationales Büro",
            "Karte", "Kimspiel", "Kluft", "Knoten",
            "Mastwurf", "Fischerknoten", "Weberknoten", "Mittelmannsknoten",
            "Rettungsschlinge", "Seilverkürzung", "Zimmermannsklang", "Kochen",
            "KompaP", "(Lager)eröffnung", "Lagerfeuer", "Lagerleiter",
            "Landesvorsitzender", "Landeswappen", "Lazarettzelt", "Mittagessen",
            "Morsezeichen", "Pfadfinder", "Pfadfinderin", "Pfadfindergruß",
            "PTA (Pfadfinder trotz allem)", "Pfadfindertum", "Pfeife", "Pfeifenschnur",
            "Pionierdienst", "Postamt", "Raider", "Ratsfelsen",
            "Redakteur", "Rover", "Roveraufbruch", "Roverstock",
            "Roverstufe", "Rucksack", "Rudel", "Rudelabzeichen",
            "Rudelführer; Leitwolf", "Rudelführerstreifen", "Schatzmeister", "Seemannskunst",
            "Seepfadfinder", "Seepfadfindermütze", "Seepfadfindertum", "Schlußzeremonie",
            "Seil", "Semarphorzeichen", "Sippe", "Sippenfarben",
            "Sippenführer", "Sippenführerabzeichen", "Sippenwimpel", "Spezialabzeichen",
            "spleißen", "Stamm", "Stammesführer", "Stammesheim",
            "Stern", "erster Stern", "zweiter Stern", "Streichhölzer", "Stufe",
            "tauschen", "Unterlager", "Versprechen", "Wahlspruch: Allzeit bereit",
            "Woodbadge", "Woodbadge-Träger", "Wegzeichen", "Wölfling",
            "Wölflingsbewegung", "Wölflingsgesetz", "Wölflingsmädchen", "Zelt", "Sippenzelt"

    };

    private String[] arrayinglese = new String[]{
            "refuse-pit", "badge", "second class badge", "first class badge", "cubmaster", "lady-cubmaster", "-",
            "troop colours", "beret", "axe", "association",
            "lashing", "national headquarters", "Chief Scout", "Chief Guide",
            "interpreter", "the latrines", "the tests", "to pass a test",
            "first aid", "flag", "breakfast", "scouter",
            "scout troop", "troop room", "troop council", "scoutmaster",
            "assistant", "grand howl", "belt", "exploring; hike",
            "scarf; neckerchief", "scarf-ring; woggle", "meeting", "shirt",
            "the second", "hat", "International Bureau", "map",
            "Kim's game", "uniform", "knot", "clove hitch",
            "fisherman's knot", "the reef", "middleman's knot", "the bowline",
            "the sheepshank", "the timber hitch", "cooking", "compass",
            "opening ceremony", "campfire", "camp chief", "country-commisioner",
            "country-emblem", "hospital tent", "dinner", "the Morse code",
            "Scout", "Girl Guide", "the salute", "handicapped scout",
            "scouting", "whistle", "lanyard", "pioneering",
            "post-office", "-", "council rock", "editor",
            "Venture Scout", "-", "thumb-stick", "venture scouting",
            "rucksack", "the six", "-", "the sixer",
            "sixer's stripes", "treasurer", "seemanship", "sea-scout",
            "sea-scout cap", "sea-scouting", "closing-ceremony", "cord",
            "semaphor code", "patrol", "patrol-colours", "patrol-leader (PL)",
            "PL's stripes", "patrol flag", "profiency badge", "to splice",
            "group", "group-scouter", "group head-quarters", "star", "first star",
            "second star", "matches", "section", "to change; to swoop",
            "sub-camp", "promise", "motto: be prepared", "woodbadge",
            "Gilwell-scouter", "conventional signs", "Cub Scout", "cubbing",
            "cub law", "Brownie", "tent", "patrol-tent"

    };

    private String[] arrayfrancese = new String[]{
            "le trou à détritus", "l'insigne", "l'insigne de seconde classe", "l'insigne de premiere classe",
            "le chef de meute", "la cheftaine de meute", "Signaux de rassemblement", "étendard; baussant",
            "le béret", "la hache", "l'association", "le brelage",
            "le quartier général", "le comissaire général", "la comissaire générale", "l'interprète",
            "les feuillés", "les épreuves", "passer une épreuve", "les premiers secours",
            "le pavillon", "le petit déjeuner", "le chef", "la troupe(m); la compagnie(f)",
            "le local troupe (de compagnie)", "le conseil de chefs (CDC)", "le chef de troupe", "l'assistant de troupe",
            "le grand hurlement", "la ceinture; le ceinturon", "l'explo(ration)", "le foulard",
            "la bague de foulard", "la réunion", "la chemise", "le second (de patrouille)",
            "le chapeau", "le bureau international", "la carte", "le jeu de Kim",
            "la tenue; l'uniforme", "le noeud", "le noeud de cabestan", "le noeud de pêcheur",
            "le noeud plat", "le noeud de milieu", "le noeud de chaise", "le noeud de jambe de chien",
            "le noeud de bois double", "la cuisin", "la boussole", "l'ouverture",
            "le feu de camp", "le chef de camp", "le commissaire de province", "l'écusson de province",
            "l'infermiere", "le dîner", "l'alphabet morse", "le scout; l'éclaireur",
            "la guide; l'éclaireuse", "le salut scout", "un scout handicapé", "le scoutisme",
            "le sifflet", "la cordelière", "le pioniérisme", "la poste",
            "l'équipier pilote", "le rocher du conseil", "le rédacteur", "le routier",
            "le départ routier", "la fourche", "la route", "le sac à dos",
            "la sizaine", "le loup de sizaine", "le sizainier", "le bandes de sizainier",
            "le trésorier", "le matelotage", "le scout marin", "le béret de scout marin",
            "le scoutisme marin", "la clôture", "la corde", "les signaux sémaphoriques",
            "la patrouille", "colouers de patrouille", "le chef de patrouille (CP)", "les bandes de CP",
            "le fanion de patrouille", "le badge", "épisser", "le groupe",
            "le chef de groupe", "le local de groupe", "l'étoile", "la premièr étoile",
            "la deuxième étoile", "les allumettes", "la branche", "échanger",
            "le sous-camp", "la promesse", "la devise:toujours prêt", "badge de bois",
            "le chef breveté", "les signes de piste", "le louveteau", "le louvetisme",
            "la loi de la jungle", "la louvette", "la tente", "la tente de patrouille"

    };

    private String[] arrayitaliano = new String[]{
            "buca dei rifiuti", "distintivo", "distintivo di seconda classe", "distintivo di prima classe",
            "Akela; capo branco", "Arcanda; capo cerchio", "segnali di chiamata", "Fiamma",
            "basco", "accetta", "associazione", "-",
            "sede nazionale", "Capo Scout", "Capo Guida", "interprete",
            "latrina", "prove; mete; obiettivi", "superare le prove", "pronto soccorso",
            "bandiera", "colazione", "capo", "reparto",
            "sede di reparto", "comunita` capi (Co.Ca.)", "capo reparto", "aiuto capo reparto",
            "grande urlo", "cintura", "escursione; hike", "fazzolettone",
            "fermafazzolettone", "riunione", "camicia", "vice capo squadriglia",
            "cappello", "Bureau Mondiale", "carta topografica", "gioco di Kim",
            "uniforme", "nodo", "nodo parlato semplice", "nodo del pescatore",
            "nodo piano", "nodo bolina", "cappio del bombardiere", "nodo margherita",
            "nodo parlato", "cucinare", "bussola", "ceremonia d'apertura",
            "fuoco di campo", "capo campo", "responsabile di zona", "distintivo regionale",
            "infermeria", "pranzo", "alfabeto Morse", "scout; esploratore",
            "guida", "saluto scout", "scout malgrado tutto (MT)", "scoutismo",
            "fischietto", "laccio per fischietto", "pionierismo", "ufficio postale",
            "novizio", "rupe del consiglio", "redattore", "rover",
            "partenza rover", "forcola; bastone rover", "roverismo", "zaino",
            "sestiglia", "distintivo di sestiglia", "capo sestiglia", "striscie da capo sestiglia",
            "tesoriere", "nautica", "scout nautico", "beretto di scout nautico",
            "scoutismo nautico", "cerimonia di chiusere", "corda", "segnali semaforici",
            "squadriglia", "colori di squadriglia", "capo squadriglia", "striscie di capo squadriglia",
            "guidone", "distintivo di specialitá", "-", "gruppo",
            "capo gruppo", "sede di gruppo", "stella", "prima stella",
            "seconda stella", "fiammiferi", "branca", "scambiare",
            "sotto campo", "promessa", "motto: estote parati", "Woodbadge",
            "capo brevettato", "segni di pista", "lupetto", "lupettismo",
            "la legge della giungla", "coccinelle", "tenda", "tenda di squadriglia",
    };

    private String[] arrayesperanto = new String[]{
            "rubotruo", "insigno", "duagrada insigno", "unuagrada insigno",
            "svarmestro", "svarmestrino", "braksignoj", "standardo",
            "mola chapo", "hakilo", "asocio", "chirkauvolvo",
            "chefsidejo", "chefkomisaro", "chefkomisarino", "interpretanto",
            "latrino", "provoj", "trapasi provon", "unua helpo",
            "flago", "mantenmangho", "trupestro", "trupo",
            "truphejmo", "trupkonsilantaro", "trupestro", "subtrupestro",
            "granda krio", "zono", "vagvojagho; esplorado", "koltuko",
            "koltuka ringo", "patrol-vespero; trup-vespero", "cemizo", "subpatrolestro",
            "chapelo", "internacia oficejo", "mapo; landkarto", "kimludo",
            "uniformo", "nodo", "mastkrocho", "fishista nodo",
            "refnodo", "mezula nodo", "sechnodo", "krurnodo",
            "lignokrocho", "kuirado", "kompaso", "inauguro",
            "tendarfajro", "tendarestro", "provinca komisaro", "provinca blazono",
            "lazareto tendo", "tagmangho", "morsa kodo", "skolto",
            "skoltino", "skolta saluto", "tamena skolto", "skoltismo",
            "fajfilo", "fajfilshnuro", "pionirservo", "poshtoficejo",
            "roveraspiranto", "konsilatara roko", "redaktoro", "rovero",
            "roverkirado", "roverbastono", "rovershtupo", "dorsosako",
            "seso", "sesinsigno", "sesestro", "sesestraj strioj",
            "trezoristo", "marista arto", "marskolto", "marista chapo",
            "marskoltismo", "fermado", "shnuro", "semafora kodo",
            "patrolo", "patrolkoloroj", "patrolestro", "patrolestraj strioj",
            "patrola standardeto", "fakinsignoj", "splisi", "grupo",
            "grupestro", "gruphejmo", "stelo", "la unua stelo",
            "la dua stelo", "alumetoj", "shtupo", "intershanghi",
            "sub-tendaro", "promeso", "divizo: chiam preta", "arbara insigno",
            "Gilwell-skolto", "vojsignoj", "lupido; skolteto", "lupida movado",
            "lupida lego", "lupidino; skoltetino", "tendo", "patroltendo"
    };

    private String[] arrayportoghese = new String[]{
            "ova de detritos", "distintivo; insígnia", "distintivo de segunda classe",
            "distintivo de primeira classe", "Akela; chefe de lobinhos", "Akela", "listel; distintivo de braço",
            "cores da tropa", "boina", "machado", "associaçao",
            "-", "sede nacional", "comissário geral", "comissária geral",
            "intérprete", "privadas; latrinas", "provas (testes) de classe", "passar em um teste",
            "primeiros socorros", "bandeira", "café de manha", "chefe", "tropa escoteira",
            "sede da tropa; local da tropa", "direçáo do grupo", "chefe de tropa", "assistente(do chefe de tropa)",
            "grande uivo", "cinto", "caminhada; excursao", "lenço",
            "anel de lenço", "reuniao", "camisa", "vice líder da patrulha",
            "chapéu", "bureau internacional", "mapa", "jogo do Kim",
            "uniforme", "nó", "fiel", "nó de pescador",
            "nó direito", "nó correr", "lais de guia", "catal",
            "volta da ribeira", "cozinhar", "bússola", "cerimônia de abertura",
            "fogo de campo", "chefe de campo", "comissário regional", "distintivo regional",
            "enfermaria; barraca de primeiros soccorros", "jantar", "código morse", "escoteiro",
            "escoteira; guia", "comprimento escoteiro; saudaçá escoteira", "aperto de máo escoteiro", "escotismo",
            "apito; assobio", "-", "pioneiria", "correio",
            "escudeiro; aspirante a pioneiro", "pedra de conselho", "redator", "pioneiro",
            "-", "forquilha", "pioneirismo", "mochila",
            "matilha", "distintivo de matilha", "(líder) chefe da matilha", "divisas do chefe da matilha",
            "tesouraria", "marinheiro", "escoteiro do mar", "boina de escoteiro do mar",
            "escotismo marinho", "ceremônia fechada", "corda", "código através de semáforo",
            "patrulha", "cores da patrulha", "(líder) chefe da patrulha", "líderes de patrulhas",
            "bandeira da patrulha", "distintivo de especialidade", "-", "grupo",
            "chefe de grupo", "sede do grupo", "estrela", "primeira estrela",
            "segunda estrela", "fósforos", "escala; degrau", "trocar",
            "sub-acampamento", "promessa", "divisa: sempre alerta", "insígnia da madeira",
            "portador da insígnia da madeira", "sinais convencionais", "lobinho", "lobismo",
            "lei de lobinhos; lei da selva", "-", "barraca; tenda", "barraca de patrulha"

    };

    private String[] arrayspagnolo = new String[]{
            "basura", "insignia", "insignia de segunda clase", "insignia de primera clase", "Akela",
            "Akela", "senales de brazos", "estandarte; guiónale tropa", "boina",
            "hacha", "asociación", "lazo", "oficina nacional",
            "comisario general", "comisaria general", "interprete", "letrinas",
            "pruebas de clase", "pasar una prueba", "primeros auxilios", "bandera",
            "desayuno", "scouter", "tropa scout", "local de tropa",
            "consejo de tropa", "jefe de tropa", "ayudante", "gran clamor",
            "cinturón", "exploración; excursión", "pañoleta", "nudo",
            "reunión", "camisa", "sub-guia de patrulla", "sombrero",
            "oficina internacional", "mapa", "juego de Kim", "uniforme",
            "nudo", "nudo ballestringue", "nudo de pescador", "nudo llano",
            "nudo correndizo", "nudo as de guia", "nudo margarita", "nudo vuelta de bnaza",
            "cocina", "brújula", "ceremonia de apertura", "fuego de campamento",
            "jefe de campamento", "comisario de area", "distintivo regional", "enfermeria",
            "cena", "código morse", "explorador", "exploradora",
            "saludo scout", "scouts impedidos", "escultismo", "silbato",
            "cordino", "pionerismo", "oficina de correos", "escudero",
            "consejo de la roca", "redactor", "rover-scout", "ceremonia pase a clan rover",
            "orquilla rover", "roverismo", "mochila", "seisena",
            "distintivo de seisena", "seisenero", "barras de seisenero", "tesoro",
            "marinero", "scouts marinos", "sombrero de scout marino", "escultismo marino",
            "ceremonia de clausura", "cuerda", "código de semáforo", "patrulla",
            "colores de patrulla", "guia de patrulla", "barras de guia de patrulla",
            "banderin de patrulla", "especialidades", "empalme", "grupo",
            "jefe de grupo", "el local de grupo", "estrella", "primera estrella",
            "segunda estrella", "fosforos; cerillas", "rama", "intercambiar",
            "sub-campo", "la promesa", "lema: siempre listos", "insignia de madera",
            "scouter-Gilwell", "signos de pista", "lobatos", "lobatismo",
            "ley de la manda", "alita; lobata", "tienda; caseta", "tienda de patrulla"
    };

    private String[] arrayolandese = new String[]{
            "afval putje", "insigne", "tweede klas insigne", "eerste klas insigne",
            "Akela", "knaagje; driempje", "-", "vlag",
            "baret", "bÿl", "vereniging", "-",
            "landelÿk bureau", "landelÿk voorzitter", "landelÿk voorzitster", "vertaler",
            "latrine", "eisen", "een proef afleggen", "eerste hulp",
            "vlag", "ontbÿt", "leider", "vendel (m); pfadfinder (f)",
            "-", "vendelraad", "gidsten(pfadfindster)leidster", "assistent",
            "gehuil", "riem", "hike; trektocht", "das",
            "dasring", "bÿeenkomst", "blouse", "assistent ronde; patrouille leider",
            "hoed", "internationaal bureau", "kaart", "kimspel",
            "uniform", "knoop", "mastworp", "vissersknoop",
            "platte knoop", "-", "-", "touwverkorting",
            "timmer steek", "koken", "kompas", "openings ceremonie",
            "kampvuur", "kamp leiding", "gewestelÿk voorzitter", "gewestelÿk embleem",
            "hospitaal tent", "middageten", "het morse alfabet", "scout; verkenner",
            "gids; padvindster", "verkennersgroet", "een blauwe vogel", "scouting",
            "fluit", "fluitekoord", "pionieren", "postkantoor",
            "-", "raadsrots", "redakteur", "rowan(m); sherpa(f)",
            "expeditie", "rowan stok", "rowan speltak; sherpa speltak", "rugzak",
            "nest", "nest insigne", "gids", "gids banden",
            "penningmeester", "-", "zee verkenner", "tok",
            "zeeverkennerÿ; watergidsen", "sluitingsceremonie", "touw", "semafoor alfabet",
            "patrouille(m); ronde(f)", "patrouille kleur; ronde kleur", "patrouille(ronde)leider(PL+RL)", "PL banden; RL banden",
            "patrouille vlag; ronde vlag", "vaardigheids insigne", "splitsen", "groep",
            "hopman", "troephuis", "ster", "eerste ster",
            "tweede ster", "lucifer", "speltak", "ruilen",
            "subcamp", "beloven", "motto: weest bereid", "woodbadge",
            "-", "-", "welp", "-",
            "welpen wet; kabouter wet", "kabouter", "tent", "patrouille tent"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        btntranslate = (Button) findViewById(R.id.btntranslate);
        edtresult = (EditText) findViewById(R.id.edtresult);

        edtresult.setKeyListener(null);
        edtresult.setCursorVisible(false);
        edtresult.setPressed(false);
        edtresult.setFocusable(false);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.language));
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                languagefrom = (String) spinner1.getSelectedItem();
                setWord(languagefrom);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Toast.makeText(MainActivity.this, "Select one item!", Toast.LENGTH_SHORT).show();
            }
        });

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources()
                .getStringArray(R.array.language));
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                languageto = (String) spinner2.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Toast.makeText(MainActivity.this, "Select one item!", Toast.LENGTH_SHORT).show();
            }
        });

        btntranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int j = -1;
                if (!word.trim().equals("")) {
                    if(!languagefrom.equals(languageto)) {
                        switch (languagefrom) {
                            case "German": {
                                switch (languageto) {
                                    case "English": {
                                        setResult(arraytedesco, arrayinglese);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arraytedesco, arrayfrancese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arraytedesco, arrayitaliano);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arraytedesco, arrayesperanto);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arraytedesco, arrayportoghese);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arraytedesco, arrayspagnolo);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arraytedesco, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "English": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayinglese, arraytedesco);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arrayinglese, arrayfrancese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arrayinglese, arrayitaliano);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arrayinglese, arrayesperanto);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arrayinglese, arrayportoghese);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arrayinglese, arrayspagnolo);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arrayinglese, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "French": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayfrancese, arraytedesco);
                                    }
                                    break;
                                    case "English": {
                                        setResult(arrayfrancese, arrayinglese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arrayfrancese, arrayitaliano);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arrayfrancese, arrayesperanto);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arrayfrancese, arrayportoghese);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arrayfrancese, arrayspagnolo);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arrayfrancese, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "Italian": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayitaliano, arraytedesco);
                                    }
                                    break;
                                    case "English": {
                                        setResult(arrayitaliano, arrayinglese);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arrayitaliano, arrayfrancese);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arrayitaliano, arrayesperanto);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arrayitaliano, arrayportoghese);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arrayitaliano, arrayspagnolo);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arrayitaliano, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "Esperanto": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayesperanto, arraytedesco);
                                    }
                                    break;
                                    case "English": {
                                        setResult(arrayesperanto, arrayinglese);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arrayesperanto, arrayfrancese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arrayesperanto, arrayitaliano);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arrayesperanto, arrayportoghese);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arrayesperanto, arrayspagnolo);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arrayesperanto, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "Portuguese": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayportoghese, arraytedesco);
                                    }
                                    break;
                                    case "English": {
                                        setResult(arrayportoghese, arrayinglese);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arrayportoghese, arrayfrancese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arrayportoghese, arrayitaliano);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arrayportoghese, arrayesperanto);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arrayportoghese, arrayspagnolo);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arrayportoghese, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "Spanish": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayspagnolo, arraytedesco);
                                    }
                                    break;
                                    case "English": {
                                        setResult(arrayspagnolo, arrayinglese);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arrayspagnolo, arrayfrancese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arrayspagnolo, arrayitaliano);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arrayspagnolo, arrayesperanto);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arrayspagnolo, arrayportoghese);
                                    }
                                    break;
                                    case "Dutch": {
                                        setResult(arrayspagnolo, arrayolandese);
                                    }
                                    break;
                                }
                            }
                            break;
                            case "Dutch": {
                                switch (languageto) {
                                    case "German": {
                                        setResult(arrayolandese, arraytedesco);
                                    }
                                    break;
                                    case "English": {
                                        setResult(arrayolandese, arrayinglese);
                                    }
                                    break;
                                    case "French": {
                                        setResult(arrayolandese, arrayfrancese);
                                    }
                                    break;
                                    case "Italian": {
                                        setResult(arrayolandese, arrayitaliano);
                                    }
                                    break;
                                    case "Esperanto": {
                                        setResult(arrayolandese, arrayesperanto);
                                    }
                                    break;
                                    case "Portuguese": {
                                        setResult(arrayolandese, arrayportoghese);
                                    }
                                    break;
                                    case "Spanish": {
                                        setResult(arrayolandese, arrayspagnolo);
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    else
                        Toast.makeText(MainActivity.this, "Select a different language!", Toast.LENGTH_SHORT).show();
                } else
                      Toast.makeText(MainActivity.this, "Insert a word to translate!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
